# Installs and configures netdata on your system.
#
# When this class is declared with the default options, Puppet:
#
# - Installs the appropriate netdata software package for your operating system.
# - Places the required configuration files in a directory, with the default location determined by your operating system.
# - Starts and the enables the netdata service.
#
# @summary Installs and configures netdata on your system.
#
# @api public
#
# @example Basic usage
#   class { 'netdata': }
#
# @param [boolean] manage_yumrepo
#   Whether the netdata package repository is managed.
#
# @param [hash] yumrepo
#   A hash of netdata package repository `yumrepo` resources to create.
#
# @param [boolean] manage_package
#  Whether the netdata package is managed.
#
# @param [string] package_name
#  The name of the netdata package.
#
# @param [string] package_ensure
#  What state the netdata package should be in.
#
# @param [boolean] manage_cfg_dir
#  Whether the netdata configuration directory is managed.
#
# @param [string] cfg_dir
#  The path to the netdata configuration directory.
#
# @param [boolean] manage_main_cfg
#  Whether the netdata main configuration file is managed.
#
# @param [string] main_cfg_file
#  The path to the netdata main configuration file.
#
# @param [hash] main_cfg
#  A hash of netdata main configuration settings.
#
# @param [boolean] manage_apps_groups_cfg
#  Whether the netdata apps.plugin process grouping configuration file is managed.
#
# @param [string] apps_groups_cfg_file
#  The path to the netdata apps.plugin process grouping configuration file.
#
# @param [hash] apps_groups_cfg
#  A hash of netdata apps.plugin process grouping configuration settings.
#
# @param [boolean] manage_charts_d_cfg
#  Whether the netdata charts.d.plugin configuration file is managed.
#
# @param [string] charts_d_cfg_file
#  The path to the netdata charts.d.plugin configuration file.
#
# @param [hash] charts_d_cfg
#  A hash of netdata charts.d.plugin configuration settings.
#
# @param [boolean] manage_fping_cfg
#  Whether the netdata fping.plugin configuration file is managed.
#
# @param [string] fping_cfg_file
#  The path to the netdata fping.plugin configuration file.
#
# @param [string] fping_bin
#  The fping binary to use for netdata fping.plugin. Use 'absent' to remove from configuration file.
#
# @param [array] fping_hosts
#  Hosts to fping with netdata fping.plugin.
#
# @param [string] fping_update_every
#  The update frequency of the chart for netdata fping.plugin. Use 'absent' to remove from configuration file.
#
# @param [string] fping_ping_every
#  The time in milliseconds to ping the hosts with netdata fping.plugin. Use 'absent' to remove from configuration file.
#
# @param [string] fping_opts
#  Other fping options for netdata fping.plugin. Use 'absent' to remove from configuration file.
#
# @param [boolean] manage_node_d_cfg
#  Whether the netdata node.d.plugin configuration file is managed.
#
# @param [string] node_d_cfg_file
#  The path to the netdata node.d.plugin configuration file.
#
# @param [hash] node_d_cfg
#  A hash of netdata node.d.plugin configuration settings.
#
# @param [boolean] manage_python_d_cfg
#  Whether the netdata python.d.plugin configuration file is managed.
#
# @param [string] python_d_cfg_file
#  The path to the netdata python.d.plugin configuration file.
#
# @param [hash] python_d_cfg
#  A hash of netdata python.d.plugin configuration settings.
#
# @param [boolean] manage_stream_cfg
#  Whether the netdata stream configuration file is managed.
#
# @param [string] stream_cfg_file
#  The path to the netdata stream configuration file.
#
# @param [boolean] stream_enabled
#  Whether this node should send metrics.
#
# @param [string] stream_destination
#  Destination for stream (`[PROTOCOL:]HOST[%INTERFACE][:PORT]`).
#  
# @param [string] stream_api_key
#  The API_KEY to use (as the sender).
#  
# @param [integer] stream_timeout
#  The timeout to connect and send metrics.
#  
# @param [integer] stream_default_port
#  Default port if not specified by `stream_destination`.
#  
# @param [integer] stream_buffer_size_bytes
#  The buffer to use for sending metrics.
#  
# @param [integer] stream_reconnect_delay
#  Number of seconds to wait before after connection failure or disconnection.
# 
# @param [integer] stream_initial_clock_resync
#  Number of clock sync iteration attempts between sending and receiving netdata hosts when starting.
#  
# @param [hash] stream_options_per_id
#  A hash of configuration settings to be applied per API_KEY or per MACHINE_GUID for netdata sending hosts on netdata receiving host.
#
# @param [boolean] manage_service
#  Whether the netdata service is managed.
#
# @param [string] service_name
#  The name of the netdata service to run.
#
# @param [string] ensure_service
#  Whether the netdata service should be running.
# 
# @param [boolean] enable_service
#  Whether the netdata service should be enabled to start at boot.
class netdata (
  $manage_yumrepo                = $netdata::params::manage_yumrepo,
  $yumrepo                       = $netdata::params::yumrepo,

  $manage_package                = $netdata::params::manage_package,
  $package_name                  = $netdata::params::package_name,
  $package_ensure                = $netdata::params::package_ensure,

  $manage_cfg_dir                = $netdata::params::manage_cfg_dir,
  $cfg_dir                       = $netdata::params::cfg_dir,

  $manage_main_cfg               = $netdata::params::manage_main_cfg,
  $main_cfg_file                 = $netdata::params::main_cfg_file,
  $main_cfg                      = $netdata::params::main_cfg,

  $manage_apps_groups_cfg        = $netdata::params::manage_apps_groups_cfg,
  $apps_groups_cfg_file          = $netdata::params::apps_groups_cfg_file,
  $apps_groups_cfg               = $netdata::params::apps_groups_cfg,

  $manage_charts_d_cfg           = $netdata::params::manage_charts_d_cfg,
  $charts_d_cfg_file             = $netdata::params::charts_d_cfg_file,
  $charts_d_cfg                  = $netdata::params::charts_d_cfg,

  $manage_fping_cfg              = $netdata::params::manage_fping_cfg,
  $fping_cfg_file                = $netdata::params::fping_cfg_file,
  $fping_bin                     = $netdata::params::fping_bin,
  $fping_hosts                   = $netdata::params::fping_hosts,
  $fping_update_every            = $netdata::params::fping_update_every,
  $fping_ping_every              = $netdata::params::fping_ping_every,
  $fping_opts                    = $netdata::params::fping_opts,

  $manage_node_d_cfg             = $netdata::params::manage_node_d_cfg,
  $node_d_cfg_file               = $netdata::params::node_d_cfg_file,
  $node_d_cfg                    = $netdata::params::node_d_cfg,

  $manage_python_d_cfg           = $netdata::params::manage_python_d_cfg,
  $python_d_cfg_file             = $netdata::params::python_d_cfg_file,
  $python_d_cfg                  = $netdata::params::python_d_cfg,

  $manage_stream_cfg             = $netdata::params::manage_stream_cfg,
  $stream_cfg_file               = $netdata::params::stream_cfg_file,
  $stream_enabled                = $netdata::params::stream_enabled,
  $stream_destination            = $netdata::params::stream_destination,
  $stream_api_key                = $netdata::params::stream_api_key,
  $stream_timeout                = $netdata::params::stream_timeout,
  $stream_default_port           = $netdata::params::stream_default_port,
  $stream_buffer_size_bytes      = $netdata::params::stream_buffer_size_bytes,
  $stream_reconnect_delay        = $netdata::params::stream_reconnect_delay,
  $stream_initial_clock_resync   = $netdata::params::stream_initial_clock_resync,
  $stream_options_per_id         = $netdata::params::stream_options_per_id,

  $manage_service                = $netdata::params::manage_service,
  $service_name                  = $netdata::params::service_name,
  $ensure_service                = $netdata::params::ensure_service,
  $enable_service                = $netdata::params::enable_service,

) inherits netdata::params {

  validate_bool($manage_yumrepo)
  validate_hash($yumrepo)

  validate_bool($manage_package)
  validate_string($package_name)
  validate_string($package_ensure)

  validate_bool($manage_cfg_dir)
  validate_absolute_path($cfg_dir)

  validate_bool($manage_main_cfg)
  validate_absolute_path($main_cfg_file)
  validate_hash($main_cfg)

  validate_bool($manage_apps_groups_cfg)
  validate_absolute_path($apps_groups_cfg_file)
  validate_array($apps_groups_cfg)

  validate_bool($manage_charts_d_cfg)
  validate_absolute_path($charts_d_cfg_file)
  validate_hash($charts_d_cfg)

  validate_bool($manage_fping_cfg)
  validate_absolute_path($fping_cfg_file)
  validate_string($fping_bin)
  validate_array($fping_hosts)
  validate_string($fping_update_every)
  validate_string($fping_ping_every)
  validate_string($fping_opts)

  validate_bool($manage_node_d_cfg)
  validate_absolute_path($node_d_cfg_file)
  validate_hash($node_d_cfg)

  validate_bool($manage_python_d_cfg)
  validate_absolute_path($python_d_cfg_file)
  validate_hash($python_d_cfg)

  validate_bool($manage_stream_cfg)
  validate_absolute_path($stream_cfg_file)
  validate_bool($stream_enabled)
  validate_string($stream_destination)
  validate_string($stream_api_key)
  validate_integer($stream_timeout)
  validate_integer($stream_default_port)
  validate_integer($stream_buffer_size_bytes)
  validate_integer($stream_reconnect_delay)
  validate_integer($stream_initial_clock_resync)
  validate_hash($stream_options_per_id)

  validate_bool($manage_service)
  validate_string($service_name)
  validate_string($ensure_service)
  validate_bool($enable_service)

  anchor { 'netdata::begin': }
  -> class { '::netdata::repo': }
  -> class { '::netdata::install': }
  -> class { '::netdata::config': }
  ~> class { '::netdata::service': }
  anchor { 'netdata::end': }
}
