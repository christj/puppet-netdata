# Private class for managing the netdata package repository.
#
# @summary Private class for managing the netdata service.
#
# @api private
#
# @example
#   include netdata::service
class netdata::service {
  if $netdata::manage_service {
    service { $netdata::service_name:
      ensure => $netdata::ensure_service,
      enable => $netdata::enable_service,
    }
  }
}
